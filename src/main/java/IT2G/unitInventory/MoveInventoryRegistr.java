package IT2G.unitInventory;

import java.util.Date;
import IT2G.HR.Employee;

//регистр перемещений
public class MoveInventoryRegistr {

    //кто перемещает
    private Employee whoMovedInventory;

    //дата перемещения
    private Date inventoryMoveDate;

    //ТМЦ
    private Inventory inventory;

    //прежнее состояние
    private StateInventory oldStateInventory;

    //новое состояние
    private StateInventory newStateInventory;

    //причина перемещения
    private CauseOfMove causeOfMove;
}
